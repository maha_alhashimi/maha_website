# **WEEK 2: Electronics Design & Production**

The second week of fab academy focused on electronics design and manufacturing. Our goals for this week were the following:

1. Produce an ISP chip using the milling machine.
2. Solder the components accordingly.
3. Program the ISP.
4. **Document everything!**


### _Electronic Design_

ISP [in-system programmer] is a chip or a component that allows in-circuit serial programming. Which is the ability to program devices while being installed in a complete system rather than obtaining a pre-programmed chip and installing it to the system.

This week's task was to fabricate and program the FabTinyISP with reference to the [tutorial](http://fab.cba.mit.edu/classes/863.16/doc/projects/ftsmin/index.html) provided.


#### Step 1: Get the Schematics right!

The first step to fabricate this particular ISP is to obtain the traces and the board outlines for the chip from provided in the tutorial.

![TRACES](Pics/traces+outlinespng.PNG)

![](pics/tracesrml.PNG)

Both pictures shown above have `.PNG` file extensions, which is not suitable, as the software used for the milling machines only reads files with `.rml` [Red line markup language file] extension.
Therefore, the files need to converted to the correct type using an online tool called [_FAB modules_](http://fabmodules.org/). The file conversion is performed as following:

1. Select the input format to be read -- **PNG** in our case.
2. Upload the PNG image from the computer.
3. Select the output format -- because we are using a Rolland milling machine we select **Roland mill (.rml)** as our option.
4. Select the process to be done -- Here we declare whether we are processing the image of the traces or the outline of the PCB. This step is done twice.

  i. **PCB Traces (1/64)** -- refers to the traces milling bit size.

  ii. **PCB Outline (1/32)** -- refers to the outline milling bit size.

5. Enter the settings for the output format:

 * Machine: SRM-20 -- The milling machine used in FABLAB UAE.
 * X0, Y0, Z0 = 0 mm.

6. click on **calculate** [note: it would take a minute or two for the file to be produced] and then **save**.

![](Pics/fabmodules.PNG)

![](pics/outputsettings.PNG)


#### Step 2: Set up the milling machine.

To fabricate the ISP chip, we are using a Roland SRM 20 machine to engrave the traces and the outlines on an FR1 [copper clad Flame Retardant 1] printed circuit board.
We are using the FR1 boards instead of the commercially found FR4 due to their safer nature, as inhaling the milling dust of an FR4 fiberglass material may lead to serious health hazards.

![](pics/rolandsrm20.PNG)

First, we mounted the FR1 plate on a medium density fiberboard (MDF) to secure the plate while milling as well to keep it levelled. The plate is adhered to the MDF using strong double sided tape where the plate is then pressed to adhere and level up properly.

![](pics/mdf.PNG)

The following steps describe the process of calibrating the machine for the milling.

1. Place the MDF board inside the Roland milling machine and secure it using the existing screws _[note: make sure to tighten diagonal screws at a time to ensure even surface]_

![](pics/securingmdf.PNG)

2. Insert the milling bit into the collet and secure it in place carefully so the milling bit doesn't fall and break the tip. we have used two different milling bits:

 a. 1/64 SE 2FL ALTIN COATED milling bit for the **traces**
 b. 1/32 SE 2FL ALTIN COATED milling bit for the **outlines**

![](pics/millingbits.PNG)

3. Using Ronald software _Vpanel for SRM-20_ on a computer connected to the machine we changed the coordinates of **X and Y** to choose a starting point (preferably at the left upper corner of the FR1 board) remembering to set the values after changing them.

![](pics/xy.PNG)

4. As for calibrating the **Z axis**, we lowered the milling bit enough for us to reach but not close enough to the board. We then un-screw the milling bit bringing its tip close to the surface to touch it slightly and then tightened it back, and set the Z value.
This step is done to ensure that the milling bit will be able to touch the surface enough to start engraving, but not be pressed into the board enough for the tip to break.


#### Step 3: Start milling!

Before going and choosing the pattern to be engraved, we can perform an experimental engrave to ensure that the milling bit tip is close enough to the surface to engrave properly.
This is done by pressing the `view` button on the software page. The milling machine launches, and if the Z axis was calibrated correctly we can observe the machine engraving an indentation on the origin point and dust  starting to gather.


Once the experimental engrave is done, we can then click on the `cut` button which opens another window. Then the existing operations should be deleted and we should add our **traces** pattern file in `.rml` and finally click on `output` to launch the machine.
The milling bit is then changed again following the previous steps of `setting up the machine` and the steps in the software are repeated to engrave the outlines.

> **Important point:**
The inner pattern [traces] should always be engraved before the outer pattern [outlines]. As engraving the outer pattern first releases the chip or the piece from its place, making it difficult to engrave the inner patterns without overly moving.

![](pics/removingpiece.PNG)


#### Step 4: Soldering!

Ahead of welding, we should prepare our stations and components used. I made sure that everything was close by in my station having good source of light and good ventilation for the welding fumes.

![](pics/station.PNG)

The components I used are listed below:

Quantity | Component |
--- |---|
1 |[Atiny 45](http://ww1.microchip.com/downloads/en/DeviceDoc/Atmel-2586-AVR-8-bit-Microcontroller-ATtiny25-ATtiny45-ATtiny85_Datasheet-Summary.pdf) 8 bit microcontroller.
2 |1k Ohm Resistors.
2 |499 Ohm Resistors.
2 |49 Ohm Resistors.
2 |3.3 Volts Zener Diodes.
1 |Green LED.
1 |Red LED.
1 |100 micro-farads Capacitor.
1 |2X3 Pin Holder.

I started soldering the components on paying attention to the orientation of some components.
For example, the **LEDs** have a slight green line on one end indicating the side connecting to ground.
The **Zener diodes** also have in indication to the ground which is a fade sliver line on one end [which can be a tad difficult to see if not under a direct source of light].
The **Atiny 45** microcontroller chip has a dot that indicates the first pin and the orientation of the chip.

while soldering, I came across times where I would apply too much solder melt on a component, thus I used the de-soldering wire to remove the extra melt and prevent any damage or short circuit.

![](pics/desolder.PNG)

And finally I checked with a multimeter for any shorts between VCC and ground points, and fortunately it yielded a success! (phew!)

![](pics/multimeter.PNG)

> **Note:**
Remove the extra layer of copper on the tip of the "USB" as it may cause a short circuit.

![](pics/finalsolder.PNG)


#### Step 5: Programming

Since the platform I'm working on is Windows 10, it means that I need to setup my development environment to be able to build the program into the board. I have used the [tutorial](http://fab.cba.mit.edu/classes/863.16/doc/projects/ftsmin/windows_avr.html) as a guide to install the toolchain into my windows system.

The programs that I have installed are the following:

1. [Atmel GNU Toolchain](https://www.microchip.com/mplab/avr-support/avr-and-arm-toolchains-c-compilers)
2. [GNU make](http://fab.cba.mit.edu/classes/863.16/doc/projects/ftsmin/make-3.81.exe)
3. [Evrdude](http://fab.cba.mit.edu/classes/863.16/doc/projects/ftsmin/avrdude-win-64bit.zip)
4. [Zadig](https://zadig.akeo.ie/)
5. [FTS firmware ](http://fab.cba.mit.edu/classes/863.16/doc/projects/ftsmin/fts_firmware_bdm_v1.zip)

I have downloaded each file/software tool and unzipped/placed them as instructed. Then I added the paths of GNU toolchain, GNU make and Everdude by going to the `control panel` >> `system and security` >> `system` >> `advanced system settings` >> `environment variable` >> double click on `paths` >> and add the new paths.

(or to make life simple, just search for `environment variables` in the search bar and follow up from there!)

> **EXTREMELY IMPORTANT NOTE:**
Copy the file path of your _own_ saved files instead of the paths provided in the tutorial as sometimes the paths can be outdated, or the files can have different titles.

Next, I have launched Zadig, checked for my device and re-installed the driver.

![](pics/zadig.PNG)

Then I had to check whether the programs were installed correctly. That is done by accessing git bash tool and typing `make  -v` to check for the GNU make software, `avr-gcc --version` to check for the GNU toolchain, and `avrdude -c usbtiny -p t45` to check for Evrdude software. However in my case, running the evrdude command doesn't show or do anything, this issue will later be discussed in the "problems faced" section.

![](pics/maketool.PNG)

Next, I opened the firmware directory in gitbash and ran `run make` to create a `.hex` file.

![](pics/make.PNG)

Once the hex file was created, I carefully plugged in my USB connector and the ISP board provided by Hashim connected along a ribbon to my own ISP board and observed as the red light on both boards went on with no problems.

The main point of connecting the two ISPs via the ribbon is that the VCC and the GND points should be connected the same along the two ends of the ribbon as shown below:

![](pics/ribbon.PNG)

Then I ran the commands `make flash` in gitbash. The command erases the target chip (my board) and programs its flash memory with the contents of the `.hex` file created previously.

Unfortunately, hence this step requires Avrdude to be completed, the process was forced off due to Avrdude not working on my laptop.
Thus, I have used Hashim's Laptop to perform this step along with the following few steps.

Next, I ran the command `make fuses` which sets all the fuses except the one that disables the reset pin.

![](pics/makeflash.PNG)

![](pics/makefuses.PNG)

To check the USB functionality, I had to plug in my own board directly and check if my computer detects the device. That is done by accessing the `device manager` and checking under `Universal Serial Bus controllers`.

The last step of programming the ISP is to blow the reset fuse by running `make rstdisbl`. This command will disable the ability to re-program the board in the future by changing the reset pin of the board to a general purpose input output pin.


To check that the ISP board is working properly as a programmer, I had a friend use it to perform the commands above to program hers and  thankfully it worked properly!



#### Problems Faced:
Because what's life without a set of challenges!

1. _Outdated program installation links_

  Some of the software required installation in windows had outdated links in the tutorial guide which lead us to manually search for the software and choose the version that best suited our computer systems.

2. _Outdated paths_

Much like the outdated installation links, the paths in the tutorial were outdated and referred to older versions of the software. Thus copying the exact path from the tutorial rendered a fail in the system recognizing the software. The solution to this is to copy the file path directly of the downloaded software files.

3. _The solder jumper_

The solder jumper! Something that I completely missed on while soldering my components! When I plugged on my ISP chip to the ribbon and the programming ISP borrowed from Hashim, I noticed that the red light was not turning on and the commands I ran rendered no results! Soon I discovered that the jumper solder was not even soldered! thus not completing the circuit and not providing power to the rest of the components. The only solution to this was to go back and solder the part, so I did just that!

4. _The Evrdude problem_

One of the main tools required to set up my ISP was not working as it should be. After re-installing the files, and checking the paths for any errors, I tried running the command to recognize the tool on Gitbash, but the results shown were nothing, not confirmation of the existence of the tool nor an error message.

Assuming that I can skip this step, I moved forward to the next few commands and was stopped again by an error message when I tried running the `make flash` command which required the usage of Evrdude tool.

![](pics/evrdudeinstallation.PNG)

![](pics/evrduderror.PNG)

The problem is apparently exists for all my collogues with a windows 10 x64 bit systems installed. The problem main cause and solution is yet to be known.
